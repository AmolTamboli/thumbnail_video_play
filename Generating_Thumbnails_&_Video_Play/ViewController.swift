//
//  ViewController.swift
//  Generating_Thumbnails_&_Video_Play
//
//  Created by Amol Tamboli on 21/06/20.
//  Copyright © 2020 Amol_Tamboli. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class ViewController: UIViewController {
    
    @IBOutlet weak var imgThumbnails: UIImageView!
    @IBOutlet weak var btnPlay: UIButton!
    let videoUrl = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4"
    
    var playerViewController = AVPlayerViewController()
    var player = AVPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnPlay.isHidden = true
        
        let url = URL(string: videoUrl)
        self.getThumbnailsFromImage(url: url!) { (thumbImage) in
            self.imgThumbnails.image = thumbImage
            self.btnPlay.isHidden = false
        }
    }
    
    //MARK:- Create Function For Thumbnails
    func getThumbnailsFromImage(url:URL, completion:@escaping ((_ image: UIImage) -> Void)){
        DispatchQueue.global().async {
            let asset = AVAsset(url: url)
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset)
            avAssetImageGenerator.appliesPreferredTrackTransform = true
            
            let thumbnailTime = CMTimeMake(value: 7, timescale: 1)
            do{
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumbnailTime, actualTime: nil)
                let thumbImage = UIImage(cgImage: cgThumbImage)
                DispatchQueue.main.async {
                    completion(thumbImage)
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    @IBAction func videoPlay(_ sender: Any) {
        playVideo()
    }
    
    //MARK:- Creating Func For Play Video
    
    func playVideo() {
        let url : URL = URL(string: videoUrl)!
        let playerView = AVPlayer(url: url as URL)
        playerViewController.player = playerView
        
        //MARK:- Present PresentViewController
        self.present(playerViewController, animated: true){
            self.playerViewController.player?.play()
        }
    }
    
}

/* MARK:- video link
 "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
 "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4",
 "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4",
 "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4",
 "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4",
 "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4",
 "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4",
 "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/Sintel.jpg",
 "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/SubaruOutbackOnStreetAndDirt.mp4",
 "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/TearsOfSteel.mp4"
 */
